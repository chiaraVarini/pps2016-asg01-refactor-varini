package utils;

import game.controller.Controller;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import static java.awt.event.KeyEvent.VK_LEFT;
import static java.awt.event.KeyEvent.VK_RIGHT;
import static java.awt.event.KeyEvent.VK_UP;

public class Keyboard implements KeyListener {

    Controller controller = Controller.getInstance();

    @Override
    public void keyPressed(KeyEvent e) {

        if(controller.mainCharacterIsAlive()){


            switch (e.getKeyCode()){

                case VK_RIGHT :  controller.backgroundShiftRight(true);
                                 break;

                case VK_LEFT :  controller.backgroundShiftRight(false);
                                break;

                case VK_UP : controller.mainCharacterJump();
                             break;
            }
        } else {
            controller.endGame();
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        controller.stopBackgroundShift();
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

}
