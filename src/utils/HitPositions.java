package utils;

/**
 * Rappresenta le possibili positizioni di scontro tra personaggi e oggetti
 * Created by chiaravarini on 15/03/17.
 */
public enum HitPositions {

    HEAD, BACK, BELOW, ABOVE;
}
