package game.model.characters;

import game.model.entities.Entity;
import utils.HitPositions;

/**
 * Represent a general Character
 * in the game
 */
public interface Character extends Entity{

    /**
     * @return true if the character is alive false otherwise
     */
    boolean isAlive();

    /**
     * @return true if the character walk to right false otherwise
     */
    boolean isToRight();

    /**
     * @return true if the charcater is moving false otherwise
     */
    boolean isMoving();

    /**
     * @param entity
     * @return true if the character is near to the entity false otherwise
     */
    boolean isNearby(final Entity entity);

    /**
     * @return the current step counter
     */
    int getCounter();

    /**
     * Set if the character is alive or dead
     * @param alive
     */
    void setAlive(boolean alive);

    /**
     * Set if the charater is moving on or not
     * @param moving
     */
    void setMoving(boolean moving);

    /**
     * Set if the character is walk to right or not
     * @param toRight
     */
    void setToRight(boolean toRight);

    /**
     *
     * @param entity
     * @param position
     * @return true if the character touching the entity at the position indicated
     */
    boolean hit(final Entity entity,final  HitPositions position);

    /**
     * @param entity
     * @return true if the character is touching the entity false otherwise
     */
    boolean contact(final Entity entity);

}