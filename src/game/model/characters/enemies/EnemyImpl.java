package game.model.characters.enemies;

import game.model.characters.BasicCharacter;
import game.model.characters.Character;
import game.model.entities.Entity;
import utils.HitPositions;

/**
 * Created by chiaravarini on 15/03/17.
 */
public abstract class EnemyImpl extends BasicCharacter implements Runnable, Character {

    private static final int PAUSE = 15;
    private final Thread t;

    public EnemyImpl(int X, int Y, int width, int height) {
        super(X, Y, width, height);
        super.setToRight(true);
        super.setMoving(true);

        t = new Thread(this);
        t.start();
    }

    @Override
    public void move(int position) {
        super.setX(super.getX() + (isToRight() ? 1 : -1));
    }

    @Override
    public void run() {
        while (true) {
            if (this.isAlive()) {
                this.move(0);
                try {
                    Thread.sleep(PAUSE);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }else {
                t.interrupt();
            }
        }
    }

    @Override
    public boolean contact(final Entity entity){
        if (this.hit(entity, HitPositions.HEAD) && this.isToRight()) {
            this.setToRight(false);
        } else if (this.hit(entity, HitPositions.BACK) && !this.isToRight()) {
            this.setToRight(true);
        }
        return false;
    }

}


