package game.model.characters;

/**
 * Rappresenta il protagonista del gioco (eg Mario)
 * Created by chiaravarini on 15/03/17.
 */
public interface MainCharacter extends  Character {

    boolean isJumping();
    void setJumping(boolean jumping);

}
