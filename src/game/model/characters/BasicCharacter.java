package game.model.characters;

import game.model.entities.Entity;
import game.model.entities.EntityImpl;
import utils.HitPositions;

public abstract class BasicCharacter extends EntityImpl implements Character{

    private static final int PROXIMITY_MARGIN = 10;
    private static final int HIT_MARGIN = 5;

    private boolean moving;
    private boolean toRight;
    private int counter;
    private boolean alive;

    public BasicCharacter(int x, int y, int width, int height) {

        super(x,y,width,height);
        this.counter = 0;
        this.moving = false;
        this.toRight = true;
        this.alive = true;
    }

    public boolean isAlive() {
        return alive;
    }

    public boolean isToRight() {
        return toRight;
    }

    public void setAlive(boolean alive) {
        this.alive = alive;
    }

    public void setMoving(boolean moving) {
        this.moving = moving;
    }

    public void setToRight(boolean toRight) {
        this.toRight = toRight;
    }

    public boolean hit(final Entity entity, final HitPositions positions){

        switch (positions){
            case ABOVE: return hitAbove(entity);
            case BACK: return hitBack(entity);
            case BELOW: return  hitBelow(entity);
            case HEAD: return hitAhead(entity);
            default: return false;
        }
    }

    public boolean isNearby(final Entity entity){
        return  ((getX() > entity.getX() - PROXIMITY_MARGIN &&
                        getX() < entity.getX() + entity.getWidth() + PROXIMITY_MARGIN) ||
                (getX()+ getWidth() > entity.getX() - PROXIMITY_MARGIN &&
                        getX() + getWidth() < entity.getX() + entity.getWidth() + PROXIMITY_MARGIN));
    }

    public boolean isMoving(){
        return  this.moving;
    }

    public int getCounter(){
        return  ++this.counter;
    }

    private boolean hitAhead(final Entity entity) {
        if (entity instanceof Character && !this.isToRight()) {
            return false;
        } else {
            return !((getX() + getWidth() < entity.getX() || getX() + getWidth() > entity.getX() + HIT_MARGIN ||
                    getY() + getHeight() <= entity.getY() || getY() >= entity.getY() + entity.getHeight()));
        }
    }

    private  boolean hitBack(final Entity entity){
        return !((getX() > entity.getX() + entity.getWidth() || getX()+ getWidth() < entity.getX() + entity.getWidth() - HIT_MARGIN ||
                getY()+ getHeight() <= entity.getY() || getY() >= entity.getY() + entity.getHeight()));
    }

    private boolean hitBelow(final Entity entity){
        int margin = HIT_MARGIN;
        if(entity instanceof Character) {
            margin = 0;
        }
        return  !((getX() + getWidth() < entity.getX() + margin || getX() > entity.getX() + entity.getWidth() - margin  ||
                getY() + getHeight() < entity.getY() || getY() + getHeight() > entity.getY() + margin));
    }

    private boolean hitAbove(final Entity entity) {
        return  !((getX() + getWidth() < entity.getX() + HIT_MARGIN || getX() > entity.getX() + entity.getWidth() - HIT_MARGIN ||
                getY() < entity.getY() + entity.getHeight() || getY() > entity.getY() + entity.getHeight() + HIT_MARGIN));
    }
}
