package game.model.characters;

import game.controller.Controller;
import game.model.entities.Entity;
import game.model.objects.GameObject;
import game.model.objects.Piece;
import utils.HitPositions;


public class Mario extends BasicCharacter implements MainCharacter {

    private static final int MARIO_OFFSET_Y_INITIAL = 243;
    private static final int FLOOR_OFFSET_Y_INITIAL = 293;
    private static final int WIDTH = 28;
    private static final int HEIGHT = 50;

    private boolean jumping;

    public Mario(int x, int y) {
        super(x, y, WIDTH, HEIGHT);
        this.jumping = false;
    }

    @Override
    public boolean isJumping() {
        return jumping;
    }

    @Override
    public void setJumping(boolean jumping) {
        this.jumping = jumping;
    }

    @Override
    public boolean contact(Entity entity) {
        if(entity instanceof Piece){
            return contactPiece((Piece)entity);

        } else if (entity instanceof GameObject){
            return contactObject((GameObject)entity);

        } else {
            return contactCharacter((Character) entity);
        }
    }

    private boolean contactObject(GameObject obj) {

        Controller controller = Controller.getInstance();

        if (this.hit(obj, HitPositions.HEAD) && this.isToRight() || this.hit(obj, HitPositions.BACK) && !this.isToRight()) {
            controller.setMovBackgroundGui(0);
            this.setMoving(false);
        }

        if (this.hit(obj, HitPositions.BELOW) && this.jumping) {
            controller.setFloorOffsetYBackgroundGui(obj.getY());

        } else if (!this.hit(obj,  HitPositions.BELOW)) {
            controller.setFloorOffsetYBackgroundGui(FLOOR_OFFSET_Y_INITIAL);

            if (!this.jumping) {
                this.setY(MARIO_OFFSET_Y_INITIAL);
            }

            if (hit(obj, HitPositions.ABOVE)) {
                controller.setHeightLimitBackgroundGui(obj.getY() + obj.getHeight()); // the new sky goes below the object

            } else if (!this.hit(obj, HitPositions.ABOVE) && !this.jumping) {
                controller.setHeightLimitBackgroundGui(0); // initial sky
            }
        }
        return false;
    }

    private boolean contactPiece(Piece piece) {
        return  (this.hit(piece, HitPositions.BACK) ||
                this.hit(piece, HitPositions.ABOVE) ||
                this.hit(piece, HitPositions.HEAD)  ||
                this.hit(piece, HitPositions.BACK));
    }

    private boolean contactCharacter(Character pers) {
        if (this.hit(pers, HitPositions.HEAD) || this.hit(pers, HitPositions.BACK)) {
            if (pers.isAlive()) {
                this.setMoving(false);
                this.setAlive(false);
            } else {
                this.setAlive(true);
            }
        } else if (this.hit(pers, HitPositions.BELOW)) {
            pers.setMoving(false);
            pers.setAlive(false);
        }
        return false;
    }
}
