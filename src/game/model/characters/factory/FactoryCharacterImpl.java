package game.model.characters.factory;

import game.model.characters.Character;
import game.model.characters.MainCharacter;
import game.model.characters.Mario;
import game.model.characters.enemies.Mushroom;
import game.model.characters.enemies.Turtle;

/**
 * Created by chiaravarini on 17/03/17.
 */
public class FactoryCharacterImpl implements FactoryCharacter {

    private static final int MarioXPosition = 300;
    private static final int MarioYPosition = 245;


    @Override
    public MainCharacter createMario() {
        return new Mario(MarioXPosition, MarioYPosition);
    }

    @Override
    public Character createMushroom(int x, int y) {
        return new Mushroom(x, y);

    }

    @Override
    public Character createTurtle(int x, int y) {
        return new Turtle(x, y);
    }
}
