package game.model.objects;

import utils.Res;
import utils.Utils;

import java.awt.*;

public class Tunnel extends GameObjectImpl {

    private static final int WIDTH = 43;
    private static final int HEIGHT = 65;
    private static final Image TUNNEL_IMAGE = Utils.getImage(Res.IMG_TUNNEL);

    public Tunnel(int x, int y) {
        super(x, y, WIDTH, HEIGHT, TUNNEL_IMAGE);
    }

}
