package game.model.objects;

import utils.Res;
import utils.Utils;

import java.awt.Image;

public class PieceImpl extends GameObjectImpl implements Piece, Runnable {

    private static final int WIDTH = 30;
    private static final int HEIGHT = 30;
    private static final int PAUSE = 10;
    private static final int FLIP_FREQUENCY = 100;
    private static final Image PIECE_IMAGE = Utils.getImage(Res.IMG_PIECE1);
    private int counter;

    public PieceImpl(int x, int y) {
        super(x, y, WIDTH, HEIGHT, PIECE_IMAGE);
    }

    public Image imageOnMovement() {
        return Utils.getImage(++this.counter % FLIP_FREQUENCY == 0 ? Res.IMG_PIECE1 : Res.IMG_PIECE2);
    }

    @Override
    public void run() {
        while (true) {
            this.imageOnMovement();
            try {
                Thread.sleep(PAUSE);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
