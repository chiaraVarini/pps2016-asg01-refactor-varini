package game.model.objects;

import utils.Res;
import utils.Utils;

import java.awt.*;

public class Block extends GameObjectImpl {

    private static final int WIDTH = 30;
    private static final int HEIGHT = 30;
    private static final Image BLOCK_IMAGE = Utils.getImage(Res.IMG_BLOCK);

    public Block(int x, int y) {
        super(x, y, WIDTH, HEIGHT, BLOCK_IMAGE);
    }

}
