package game.model.objects;

import java.awt.Image;

import game.model.entities.EntityImpl;

public class GameObjectImpl extends EntityImpl implements GameObject {

    private Image imgObject;

    public GameObjectImpl(int x, int y, int width, int height, Image imageObject) {

        super(x,y,width,height);
        this.imgObject = imageObject;
    }

    public Image getImgObj() {
        return imgObject;
    }

}
