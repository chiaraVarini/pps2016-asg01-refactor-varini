package game.model.objects;

import game.model.entities.Entity;

import java.awt.*;

/**
 * Represent a generic object int he game
 * Created by chiaravarini on 15/03/17.
 */
public interface GameObject extends Entity {

    /**
     * @return the image of the object
     */
     Image getImgObj();

}
