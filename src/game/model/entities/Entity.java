package game.model.entities;

/**
 * Represent a generic entity in the game
 * that has some dimension and position
 * Created by chiaravarini on 15/03/17.
 */
public interface Entity {

    /**
     * @return the x position of entity
     */
    int getX();

    /**
     * @return the y position of entity
     */
    int getY();

    /**
     * @return the width of entity
     */
    int getWidth();

    /**
     * @return the height of entity
     */
    int getHeight();

    /**
     *Set x position of entity
     * @param x
     */
    void setX(final int x);

    /**
     *Set y position of entity
     * @param y
     */
    void setY(final int y);

    /**
     *Set width of entity
     * @param width
     */
    void setWidth(final int width);

    /**
     * Set height of entity
     * @param height
     */
    void setHeight(final int height);

    /**
     * Move the position of entity to "position"
     * @param position
     */
    void move(int position);
}
