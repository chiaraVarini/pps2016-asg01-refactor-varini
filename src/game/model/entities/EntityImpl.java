package game.model.entities;

/**
 * Created by chiaravarini on 15/03/17.
 */
public class EntityImpl implements Entity{

    private int width, height;
    private int x, y;

    public EntityImpl(final int x, final  int y, final int width, final  int height){
        this.width=width;
        this.height = height;
        this.x = x;
        this.y = y;
    }

    @Override
    public int getX() {
        return this.x;
    }

    @Override
    public int getY() {
        return this.y;
    }

    @Override
    public int getWidth() {
        return this.width;
    }

    @Override
    public int getHeight() {
        return this.height;
    }

    @Override
    public void setWidth(int width) {
        this.width = width;
    }

    @Override
    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public void setX(int x) {
        this.x = x;
    }

    @Override
    public void setY(int y) {
        this.y = y;
    }

    @Override
    public void move(int pos) {
        setX(getX() - pos);
    }
}
