package game.controller;

import game.model.characters.Character;
import game.model.characters.factory.FactoryCharacter;
import game.model.characters.factory.FactoryCharacterImpl;
import game.model.objects.*;
import game.model.characters.*;

import java.awt.Graphics;
import java.util.*;

import utils.Res;
import utils.Utils;
import game.view.character.EnemyGui;
import game.view.character.MarioGui;
import game.view.character.MushroomGui;
import game.view.character.TurtleGui;

/**
 * Controller of Charcters
 * that managed the change of position of them
 */

@SuppressWarnings("serial")
public class ControllerCharacter {

    private static ControllerCharacter SINGLETON;
    private MainCharacter mario;
    private MarioGui marioGui;
    private Map<Character, EnemyGui> enemies;

    public static ControllerCharacter getInstance(){
        if(SINGLETON == null){
            SINGLETON = new ControllerCharacter();
        }
        return SINGLETON;
    }

    private ControllerCharacter() {

        super();
        FactoryCharacter factory = new FactoryCharacterImpl();
        this.mario = factory.createMario();
        this.enemies = new HashMap<>();
        this.enemies.put(factory.createMushroom(800, 263), new MushroomGui());
        this.enemies.put(factory.createMushroom(850, 263), new MushroomGui());
        this.enemies.put(factory.createMushroom(900, 263), new MushroomGui());
        this.enemies.put(factory.createTurtle(950, 243), new TurtleGui());
        this.enemies.put(factory.createTurtle(1000, 243), new TurtleGui());
        this.enemies.put(factory.createTurtle(1110, 243), new TurtleGui());
        this.marioGui = new MarioGui();

    }

    public MainCharacter getMainCharacter(){
        return this.mario;
    }

    public List getEnemies(){
        return Collections.unmodifiableList(new ArrayList(this.enemies.keySet()));
    }

    public void moveRightMainCharacter(boolean right){
        getMainCharacter().setMoving(true);
        getMainCharacter().setToRight(right);
    }

    public void setMainCharacterJumping(boolean jump){
        getMainCharacter().setJumping(jump);
    }

    public void stopWalk(){
        getMainCharacter().setMoving(false);
    }

    public void drawCharacter(Graphics g){
        if (this.mario.isJumping())
            g.drawImage(this.marioGui.doJump(), this.mario.getX(), this.mario.getY(), null);
        else
            g.drawImage(this.marioGui.walk(this.mario.isMoving(), this.mario.isToRight(), this.mario.getCounter()), this.mario.getX(), this.mario.getY(), null);

        this.enemies.forEach((enemy,enemyGui) -> {
            if(enemy.isAlive()) {
                g.drawImage(enemyGui.walk(enemy.isMoving(), enemy.isToRight(), enemy.getCounter()), enemy.getX(), enemy.getY(), null);
            }else {
                g.drawImage( enemyGui.deadImage(), enemy.getX(), enemy.getY() + enemyGui.getDeadOffset(), null);

            }
        });
    }

    public void moveEnemy(int position){
        this.enemies.keySet().forEach(e-> e.move(position));
    }

    public void modifyPosition(){

        ControllerObjects controllerObject = ControllerObjects.getInstance();
        controllerObject.getObjects().forEach(object->{
            if (this.mario.isNearby(object)) {
                this.mario.contact(object);
            }
            this.enemies.keySet().forEach(enemy -> {
                if (enemy.isNearby(object))
                    enemy.contact(object);
            });
        });

        List<Piece> pieces = controllerObject.getPieces();

        for (int i = 0; i < pieces.size(); i++) {
            if (this.mario.contact(controllerObject.getPiece(i))) {
                Utils.playSound(Res.AUDIO_MONEY);
                controllerObject.removePiece(i);
            }
        }

        this.enemies.keySet().forEach(enemy -> {

            this.enemies.keySet().forEach(otherCharacter ->{
                if(!enemy.equals(otherCharacter)){
                    if (enemy.isNearby(otherCharacter)) {
                        enemy.contact(otherCharacter);
                    }
                }
            });

            if(this.mario.isNearby(enemy)){
                this.mario.contact(enemy);
            }
        });
    }
}
