package game.controller;

import game.view.BackgroundGui;
import game.view.EndPanel;

import javax.swing.*;

/**
 * Conrtoller that menaged the repaint of gui
 * Created by chiaravarini on 17/03/17.
 */
public class ControllerGui {

    private static ControllerGui SINGLETON;
    private BackgroundGui backgroundGui;

    public static ControllerGui getInstance(){
        if(SINGLETON == null){
            SINGLETON = new ControllerGui();
        }
        return SINGLETON;
    }

    private ControllerGui(){
        this.backgroundGui = new BackgroundGui();
    }

    public void shiftBackgorundToRight(){
        if (backgroundGui.getxPos() == -1) {
            backgroundGui.setxPos(0);
            backgroundGui.setBackground1PosX(-50);
            backgroundGui.setBackground2PosX(750);
        }
    }

    public void shiftBackgorundToLeft() {
        if (backgroundGui.getxPos() == 4601) {
            backgroundGui.setxPos(4600);
            backgroundGui.setBackground1PosX(-50);
            backgroundGui.setBackground2PosX(750);
        }
    }

    public void stopShift(){
        backgroundGui.setMov(0);
    }

    public BackgroundGui getBackgroundGui(){
        return this.backgroundGui;
    }

    public JPanel endGamePanel(){
        return new EndPanel();
    }

}
