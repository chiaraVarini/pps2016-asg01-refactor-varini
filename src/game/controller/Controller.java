package game.controller;

import utils.Res;
import utils.Utils;
import game.view.GameFrame;

import javax.swing.*;
import java.awt.*;

/**
 * Controller of all application
 * Created by chiaravarini on 17/03/17.
 */
public class Controller {

    private static Controller SINGLETON = new Controller();
    private static final int THREAD_PAUSE = 3;

    private static final ControllerCharacter controllerCharacter = ControllerCharacter.getInstance();
    private static final ControllerObjects controllerObjects =  ControllerObjects.getInstance();
    private static final ControllerGui controllerGui = ControllerGui.getInstance();

    private boolean start = true;
    private JFrame window;

    public static  Controller getInstance(){
        if(SINGLETON == null){
            SINGLETON = new Controller();
        }
        return SINGLETON;
    }

    private Controller(){}

    public void startGame(){
        window = new GameFrame();
        window.setContentPane(controllerGui.getBackgroundGui());

        window.setVisible(true);
        start = true;

        new Thread(() -> {
            while (start) {
                controllerGui.getBackgroundGui().repaint();
                try {
                    Thread.sleep(THREAD_PAUSE);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public boolean mainCharacterIsAlive(){
        return controllerCharacter.getMainCharacter().isAlive();
    }

    public void mainCharacterJump(){
        controllerCharacter.setMainCharacterJumping(true);
        Utils.playSound(Res.AUDIO_JUMP);
    }

    public boolean mainCharacterToRight(){
        return controllerCharacter.getMainCharacter().isToRight();
    }

    public void setMainCharacterJump(boolean jump){
        controllerCharacter.setMainCharacterJumping(jump);
    }

    public int getMainCharacterDimension(String dimension){
        if(dimension.equals("height")){
            return controllerCharacter.getMainCharacter().getHeight();
        }else if(dimension.equals("width")){
            return controllerCharacter.getMainCharacter().getWidth();
        }
        return Integer.MIN_VALUE;
    }

    public int getMainCharacterPositon(String position){
        if(position.equals("x")){
            return controllerCharacter.getMainCharacter().getX();
        }else if(position.equals("y")){
            return controllerCharacter.getMainCharacter().getY();
        }
        return Integer.MIN_VALUE;
    }

    public void setMainCharacterPosition(String position, int value){
        if(position.equals("x")){
            controllerCharacter.getMainCharacter().setX(value);
        }else if(position.equals("y")){
            controllerCharacter.getMainCharacter().setY(value);
        }
    }

    public void modifyPosition(){
        controllerCharacter.modifyPosition();
    }

    public void drawCharacter(Graphics g){
        controllerCharacter.drawCharacter(g);
    }

    public void drawObject(Graphics g){
        controllerObjects.drawObject(g);
    }

    public void backgroundShiftRight(boolean right){

        if (right){
            controllerGui.shiftBackgorundToRight();
            controllerCharacter.moveRightMainCharacter(right);
            controllerGui.getBackgroundGui().setMov(1);

        }else {
            controllerGui.shiftBackgorundToLeft();
            controllerCharacter.moveRightMainCharacter(right);
            controllerGui.getBackgroundGui().setMov(-1);
        }
    }

    public void stopBackgroundShift(){
        controllerCharacter.stopWalk();

        controllerGui.stopShift();
    }

    public void moveCamera(int position){
        controllerObjects.moveObject(position);
        controllerCharacter.moveEnemy(position);
    }

    public void setMovBackgroundGui(int mov){
        controllerGui.getBackgroundGui().setMov(mov);
    }

    public void setFloorOffsetYBackgroundGui(int floorOffset){
        controllerGui.getBackgroundGui().setFloorOffsetY(floorOffset);
    }
    public void setHeightLimitBackgroundGui(int heightLimit){
        controllerGui.getBackgroundGui().setHeightLimit(heightLimit);
    }

    public int getHeightLimit(){
        return controllerGui.getBackgroundGui().getHeightLimit();
    }

    public int getFloorOffsetY(){
        return  controllerGui.getBackgroundGui().getFloorOffsetY();
    }

    public void endGame(){
        this.start = false;
        window.setContentPane(controllerGui.endGamePanel());
        window.revalidate();
        window.repaint();
    }

}
