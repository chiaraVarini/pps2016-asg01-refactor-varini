package game.controller;

import game.model.objects.*;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Controller of GameObjects
 * that managed the change of position of them
 */

public class ControllerObjects {

    private static ControllerObjects SINGLETON;
    private List<GameObject> objects;
    private List<Piece> pieces;

    public static ControllerObjects getInstance(){
        if(SINGLETON == null){
            SINGLETON = new ControllerObjects();
        }
        return SINGLETON;
    }

    private ControllerObjects(){
        FactoryObject factory = new FactoryObjectImpl();
        objects = new ArrayList<GameObject>();

        this.objects.add(factory.createTunnel(600, 230));
        this.objects.add(factory.createTunnel(1000, 230));
        this.objects.add(factory.createTunnel(1600, 230));
        this.objects.add(factory.createTunnel(1900, 230));
        this.objects.add(factory.createTunnel(2500, 230));
        this.objects.add(factory.createTunnel(3000, 230));
        this.objects.add(factory.createTunnel(3800, 230));
        this.objects.add(factory.createTunnel(4500, 230));

        this.objects.add(factory.createBlock(400, 180));
        this.objects.add(factory.createBlock(1200, 180));
        this.objects.add(factory.createBlock(1270, 170));
        this.objects.add(factory.createBlock(1340, 160));
        this.objects.add(factory.createBlock(2000, 180));
        this.objects.add(factory.createBlock(2600, 160));
        this.objects.add(factory.createBlock(2650, 180));
        this.objects.add(factory.createBlock(3500, 160));
        this.objects.add(factory.createBlock(3550, 140));
        this.objects.add(factory.createBlock(4000, 170));
        this.objects.add(factory.createBlock(4200, 200));
        this.objects.add(factory.createBlock(4300, 210));

        pieces = new ArrayList<>();
        this.pieces.add(factory.createPiece(402, 145));
        this.pieces.add(factory.createPiece(1202, 140));
        this.pieces.add(factory.createPiece(1272, 95));
        this.pieces.add(factory.createPiece(1342, 40));
        this.pieces.add(factory.createPiece(1650, 145));
        this.pieces.add(factory.createPiece(2650, 145));
        this.pieces.add(factory.createPiece(3000, 135));
        this.pieces.add(factory.createPiece(3400, 125));
        this.pieces.add(factory.createPiece(4200, 145));
        this.pieces.add(factory.createPiece(4600, 40));
    }

    public void moveObject(int position){
        this.objects.forEach(o->o.move(position));
        this.pieces.forEach(p->p.move(position));
    }

    public  void drawObject(Graphics g){
        this.objects.forEach(object -> g.drawImage(object.getImgObj(), object.getX(), object.getY(), null));
        this.pieces.forEach(piece -> g.drawImage(piece.imageOnMovement(), piece.getX(), piece.getY(), null));
    }


    public List<GameObject> getObjects(){
        return Collections.unmodifiableList(this.objects);
    }

    public List<Piece> getPieces(){
        return Collections.unmodifiableList(this.pieces);
    }

    public Piece getPiece(int index){
        return this.pieces.get(index);
    }

    public void removePiece(int index){
        this.pieces.remove(index);
    }

}
