package game.view.character;

import utils.Res;

/**
 * Created by chiaravarini on 18/03/17.
 */
public class TurtleGui extends EnemyGui {

    private static final int TURTLE_DEAD_OFFSET_Y = 30;

    public TurtleGui() {

        super(Res.IMGP_CHARACTER_TURTLE, Res.IMG_TURTLE_DEAD);
    }

    public int getDeadOffset(){
        return  TURTLE_DEAD_OFFSET_Y;
    }
}
