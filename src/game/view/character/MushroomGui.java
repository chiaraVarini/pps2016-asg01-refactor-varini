package game.view.character;

import utils.Res;

/**
 * Created by chiaravarini on 18/03/17.
 */
public class MushroomGui extends EnemyGui {

    private static final int MUSHROOM_DEAD_OFFSET_Y = 20;

    public MushroomGui() {
        super( Res.IMGP_CHARACTER_MUSHROOM, Res.IMG_MUSHROOM_DEAD_SX);
    }

    public int getDeadOffset(){
        return  MUSHROOM_DEAD_OFFSET_Y;
    }

}
