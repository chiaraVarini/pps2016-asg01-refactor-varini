package game.view.character;

import game.controller.Controller;
import game.view.character.GuiCharacter;
import utils.Res;
import utils.Utils;

import java.awt.*;

/**
 * Created by chiaravarini on 18/03/17.
 */
public abstract class EnemyGui implements GuiCharacter {

    private static final int FREQUENCY = 45;

    private final String nameImage;
    private final String nameDeadImage;
    private final Controller controller = Controller.getInstance();

    public EnemyGui(final String nameImage, final String nameDeadImage ){
        this.nameImage = nameImage;
        this.nameDeadImage = nameDeadImage;
    }


    @Override
    public Image walk(boolean moving, boolean toRight, int counter) {

        boolean justFrequency = counter % FREQUENCY == 0;

        String str = Res.IMG_BASE + nameImage +
                (!moving || justFrequency ? Res.IMGP_STATUS_ACTIVE : Res.IMGP_STATUS_NORMAL) +
                (toRight ? Res.IMGP_DIRECTION_DX : Res.IMGP_DIRECTION_SX) +
                Res.IMG_EXT;

        return Utils.getImage(str);

    }

    @Override
    public Image getImage() {
        return Utils.getImage(this.nameImage);
    }

    @Override
    public Image deadImage() {
        return Utils.getImage(this.nameDeadImage);
    }

    @Override
    public Image doJump() {
        return null;
    }

}
