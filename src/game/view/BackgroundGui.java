package game.view;

import game.controller.Controller;
import utils.Res;
import utils.Utils;

import javax.swing.*;
import java.awt.*;


/**
 * Questa classe mantiene tutte le informazioni della scena di backgorund
 * e di come deve aggiornarsi duranye il gioco
 * Created by chiaravarini on 17/03/17.
 */
public class BackgroundGui extends JPanel {

    private static final int FLAG_X_POS = 4650;
    private static final int CASTLE_X_POS = 4850;
    private static final int FLAG_Y_POS = 115;
    private static final int CASTLE_Y_POS = 145;

    private Image imgBackground1;
    private Image imgBackground2;
    private Image castle;
    private Image start;
    private Image imgFlag;
    private Image imgCastle;

    private int background1PosX;
    private int background2PosX;
    private int mov;
    private int xPos;
    private int floorOffsetY;
    private int heightLimit;


    public BackgroundGui(){
        super();
        this.background1PosX = -50;
        this.background2PosX = 750;
        this.mov = 0;
        this.xPos = -1;
        this.floorOffsetY = 293;
        this.heightLimit = 0;
        this.imgBackground1 = Utils.getImage(Res.IMG_BACKGROUND);
        this.imgBackground2 = Utils.getImage(Res.IMG_BACKGROUND);
        this.castle = Utils.getImage(Res.IMG_CASTLE);
        this.start = Utils.getImage(Res.START_ICON);

        this.imgCastle = Utils.getImage(Res.IMG_CASTLE_FINAL);
        this.imgFlag = Utils.getImage(Res.IMG_FLAG);

    }

    public int getFloorOffsetY() {
        return floorOffsetY;
    }

    public int getHeightLimit() {
        return heightLimit;
    }

    public int getMov() {
        return mov;
    }

    public int getxPos() {
        return xPos;
    }

    public void setBackground2PosX(int background2PosX) {
        this.background2PosX = background2PosX;
    }

    public void setFloorOffsetY(int floorOffsetY) {
        this.floorOffsetY = floorOffsetY;
    }

    public void setHeightLimit(int heightLimit) {
        this.heightLimit = heightLimit;
    }

    public void setxPos(int xPos) {
        this.xPos = xPos;
    }

    public void setMov(int mov) {
        this.mov = mov;
    }

    public void setBackground1PosX(int x) {
        this.background1PosX = x;
    }

    private void updateBackgroundOnMovement() {
        if (this.xPos >= 0 && this.xPos <= 4600) {
            this.xPos = this.xPos + this.mov;  // Moving the screen to give the impression that Mario is walking
            this.background1PosX = this.background1PosX - this.mov;
            this.background2PosX = this.background2PosX - this.mov;
        }

        if(background1PosX == 800 || background1PosX == -800){ // Flipping between background1 and background2
            this.background1PosX = -this.background1PosX;
        }
        if(background2PosX == 800 || background2PosX == -800) {
            this.background2PosX = -this.background2PosX;
        }
    }

    public void paintComponent(Graphics g){
        super.paintComponent(g);
        Graphics g2 = g;

        Controller controller = Controller.getInstance();

        controller.modifyPosition();

        updateBackgroundOnMovement();
        if(this.xPos >= 0 && this.xPos <= 4600){
            controller.moveCamera(getMov());
        }

        g2.drawImage(this.imgBackground1, this.background1PosX, 0, null);
        g2.drawImage(this.imgBackground2, this.background2PosX, 0, null);
        g2.drawImage(this.castle, 10 - this.xPos, 95, null);
        g2.drawImage(this.start, 220 - this.xPos, 234, null);

        controller.drawObject(g2);

        g2.drawImage(this.imgFlag, FLAG_X_POS - this.xPos, FLAG_Y_POS, null);
        g2.drawImage(this.imgCastle, CASTLE_X_POS - this.xPos, CASTLE_Y_POS, null);

        controller.drawCharacter(g2);
    }

}
