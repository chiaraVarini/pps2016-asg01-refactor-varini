package game.view;

import javax.swing.*;
import java.awt.*;

import static java.lang.System.exit;

/**
 * Created by chiaravarini on 18/03/17.
 */
public class EndPanel extends JPanel {

    public EndPanel(){
        setBackground(Color.black);
        setLayout(new BorderLayout());
        JLabel label = new JLabel(" YOU LOST!");
        label.setForeground(Color.white);

        Font labelFont = label.getFont();
        label.setFont(new Font(labelFont.getName(), Font.PLAIN, 30));

        JPanel p = new JPanel();
        p.setBackground(Color.black);
        p.add(label);

        JButton button = new JButton("Exit");
        button.addActionListener(e->exit(1));

        this.add(p, BorderLayout.CENTER);
        this.add(button, BorderLayout.SOUTH);
    }
}
