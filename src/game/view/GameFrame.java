package game.view;

import utils.Keyboard;

import java.awt.*;
import javax.swing.*;

public class GameFrame extends JFrame {

    private static final int WINDOW_WIDTH = Toolkit.getDefaultToolkit().getScreenSize().width / 2;
    private static final int WINDOW_HEIGHT = (int) (Toolkit.getDefaultToolkit().getScreenSize().height / 2.5);
    private static final String WINDOW_TITLE = "Super Mario";

    public GameFrame() {
        this.setTitle(WINDOW_TITLE);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        this.setLocationRelativeTo(null);
        this.setResizable(true);
        this.setAlwaysOnTop(true);
        this.setFocusable(true);
        this.requestFocusInWindow();

        this.addKeyListener(new Keyboard());
    }
}
